# Data Processing Pipeline for AI Projects

Welcome to our AI project's repository! This README provides a detailed guide on our data processing pipeline, ensuring clarity and reproducibility in our workflows. Here, you will find step-by-step instructions on how we handle data cleaning, feature extraction, feature engineering, data splitting, and versioning.

## Table of Contents
- [Project Overview](#project-overview)
- [Data Processing Pipeline](#data-processing-pipeline)
  - [Data Cleaning](#data-cleaning)
  - [Feature Extraction](#feature-extraction)
  - [Feature Engineering](#feature-engineering)
  - [Data Splitting](#data-splitting)
  - [Data Versioning](#data-versioning)
- [Getting Started](#getting-started)
- [Contributing](#contributing)
- [License](#license)

## Project Overview
Dalam domain analisis teks, klasifikasi dokumen merupakan tantangan yang sering dihadapi, khususnya dalam mengelola volume data yang besar. Proyek ini menangani klasifikasi teks menggunakan dataset BBC yang terdiri dari ribuan file teks yang telah tersegresi ke dalam kelas-kelas yang berbeda. Masing-masing kelas mewakili kategori berita tertentu. Klasifikasi yang akurat dan efisien dari dokumen-dokumen ini menjadi penting untuk pengelolaan informasi yang lebih baik dan akses yang cepat terhadap konten yang relevan. Klasifikasi ini harus mengatasi berbagai tantangan seperti pengelolaan teks dalam jumlah besar, pengurangan dimensi, dan pengambilan fitur yang efektif untuk meningkatkan performa prediksi.

## Goals
- Pengembangan Model Klasifikasi: Mengembangkan model machine learning yang dapat secara akurat mengklasifikasikan teks-teks berita ke dalam kategori yang relevan seperti politik, bisnis, olahraga, dll.
- Optimalisasi dan Evaluasi Model: Melakukan tuning parameter pada model yang dipilih untuk meningkatkan akurasi klasifikasi. Model harus dievaluasi menggunakan metrik seperti akurasi, presisi, recall, dan F1-score.
- Implementasi Reduksi Dimensi: Menerapkan teknik seperti PCA atau t-SNE jika diperlukan untuk mengurangi dimensi data tanpa mengorbankan informasi kritikal, sehingga meningkatkan efisiensi pemrosesan.
- Automasi Preprocessing Data: Mengembangkan pipeline preprocessing yang dapat secara otomatis melakukan tokenisasi, stopword removal, dan vektorisasi teks.
- Interpretasi Hasil: Memberikan wawasan tentang pengelompokan kelas dan menentukan fitur-fitur kunci yang paling mempengaruhi prediksi klasifikasi.
- Deployment Model: Menyiapkan model yang telah dikembangkan agar dapat digunakan dalam aplikasi produksi yang membutuhkan klasifikasi teks secara real-time.
Dengan mencapai tujuan ini, proyek akan memberikan solusi yang robust untuk klasifikasi teks berbasis kategori berita, mempercepat proses pengambilan informasi yang relevan dan meningkatkan manajemen konten berbasis teks.

## Data Processing Pipeline

### Data Cleaning
**Objective:** Ensure that the data is free of errors or inconsistencies, and is ready for analysis and modeling.

**Steps:**
1. **Identify the .txt document dataset and its contents.**
2. **Preprocess Data using Regex**

**Tools & Libraries:**
- Pandas for data manipulation
- NumPy for numerical operations
- Scikit-learn for preprocessing

### Feature Extraction
**Objective:** Transform raw data into meaningful features suitable for model building.

**Steps:**
1. **Text Data:** Use NLP techniques to extract features from text such as bag of words, TF-IDF, etc. In this case, we're using TF-IDF.

**Tools & Libraries:**
- NLTK/SpaCy for natural language processing

### Feature Engineering
**Objective:** Enhance the predictive power of the input data by creating new features or modifying existing ones.

**Steps:**
1. **WordNetLemmatizer**

**Tools & Libraries:**
- Scikit-learn for creating and selecting features

### Data Splitting
**Objective:** Divide the data into training, validation, and test sets to ensure robust model evaluation.

**Steps:**
1. **Train & Test Split:** : Train Test split -> 0.25

**Tools & Libraries:**
- Scikit-learn for splitting data

### Data Versioning
**Objective:** Keep track of different versions of datasets used in the project.

**Steps:**
1. **Version Control:** Use tools like GitLab/DVC to manage and version control datasets.
2. **Documentation:** Document the changes in each version, including the rationale for changes and impact on models.

**Tools & Libraries:**
- GitLab

## Getting Started
Provide instructions on how to set up and run the project locally. This section should include:
- Installation of required libraries
- How to run the scripts
- Example commands

```bash
# Clone the repository
git clone https://gitlab.com/rickyig_project/bbc-text-classification-rickyig.git
cd bbc-text-classification-rickyig

# Run the notebook inside the code folder
```

## Contributing
Encourage other developers to contribute to your project by providing guidelines on how they can contribute.

## License
Specify the license under which your project is made available. This informs users of what they can and cannot do with your code.
