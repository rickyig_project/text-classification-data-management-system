import numpy as np
import pandas as pd
import os
from sklearn.metrics import confusion_matrix,classification_report,accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV,RandomizedSearchCV
from sklearn.cluster import KMeans , AgglomerativeClustering, DBSCAN
from sklearn.metrics import silhouette_score
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from string import punctuation
from unidecode import unidecode
from contractions import fix
from nltk.util import ngrams
from sklearn.feature_extraction.text import TfidfVectorizer,CountVectorizer
from nltk.stem import LancasterStemmer, WordNetLemmatizer
from sklearn.linear_model import SGDClassifier, LogisticRegression
from sklearn.naive_bayes import MultinomialNB, GaussianNB, BernoulliNB
from sklearn.svm import SVC
import re
import nltk
import pickle

nltk.download('punkt')
nltk.download('wordnet')

## Load Data

# sample_path = "data/bbc-full-text-document-classification/bbc/business/001.txt"
def read_extract_text_file(path):
    with open(path,'r',encoding='latin-1') as file:
        data = file.readlines()
        text_data= " ".join(data)
    return text_data
# print(read_extract_text_file(sample_path))

class_labels = {'politics':0, 'sport':1,  'tech':2, 'entertainment':3, 'business':4}

path = "data/bbc-full-text-document-classification/bbc"
folder = os.listdir(path)

final_text = []
final_label = []

for label in folder:
    #print(label)
    new_path = os.path.join(path,label)
    if new_path.endswith("README.TXT"):
        #print(new_path)
        continue
    else:
        for j in os.listdir(new_path):
            #print(j)
            new_path1 = os.path.join(new_path,j)
            text = read_extract_text_file(new_path1)
            final_text.append(text)
            final_label.append(class_labels[label])

print(len(final_text))
print(len(final_label))

print(final_label[:5])

df = pd.DataFrame()
df['Text'] = final_text
df['Label'] = final_label

print(df)

df.to_csv("data/init_df_file.csv",index=False)

print(df['Label'].value_counts())

## Data Preprocessing
stopwords_list = stopwords.words('english')
len(stopwords_list)

def preprocess_data(text):
    text = text.lower()
    text = text.replace("\n"," ").replace("\t"," ")
    text = re.sub("\s+"," ",text)
    text = re.sub(r'\d+', '', text)
    text = re.sub(r'[^\w\s]', '', text)
    
    # tokens
    tokens = word_tokenize(text)
    
    data = [i for i in tokens if i not in punctuation]
    data = [i for i in data if i not in stopwords_list]
    
    # Lemmatization
    lemmatizer = WordNetLemmatizer()
    final_text = []
    for i in data:
        word = lemmatizer.lemmatize(i)
        final_text.append(word)
        
    return " ".join(final_text)

## Split The Data For Train & Test 
X_train, X_test, y_train, y_test = train_test_split(df['Text'],df['Label'],test_size=0.25, shuffle=True)
print(X_train.shape)
print(X_test.shape)
print(y_train.shape)
print(y_test.shape)

clean_train = X_train.apply(preprocess_data)
clean_test = X_test.apply(preprocess_data)

clean_train.to_csv("data/Xtrain_df_file.csv",index=False)
y_train.to_csv("data/ytrain_df_file.csv",index=False)

clean_test.to_csv("data/Xtest_df_file.csv",index=False)
y_test.to_csv("data/ytest_df_file.csv",index=False)

## TF-IDF
tfidf = TfidfVectorizer(ngram_range=(1,5),max_df=0.95, max_features=15000)

tfidf_train = tfidf.fit_transform(clean_train)
tfidf_test = tfidf.transform(clean_test)

with open("data/vectorizer.pickle", "wb") as f:
    pickle.dump(tfidf, f)

train_data = {"x":tfidf_train, "y":y_train}
test_data = {"x":tfidf_test, "y":y_test}

with open("data/train_data.pickle", "wb") as f:
    pickle.dump(train_data, f)
    
with open("data/test_data.pickle", "wb") as f:
    pickle.dump(test_data, f)

